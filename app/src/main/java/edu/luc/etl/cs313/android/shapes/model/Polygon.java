package edu.luc.etl.cs313.android.shapes.model;
import java.util.Iterator;
import java.util.List;

/**
 * A special case of a group consisting only of Points.
 *
 */
public class Polygon extends Group {

	public Polygon(Point... points) {
		super(points);
	}

	@SuppressWarnings("unchecked")
	public List<? extends Point> getPoints() {
		return (List<? extends Point>) getShapes();
	}
	@Override
	public <Result> Result accept(final Visitor<Result> v) {
		return v.onPolygon(this);
	}

	public int getMaxX(){
		final Iterator<? extends Point> point = getPoints().iterator();
		Location current = point.next();
		int max = current.getX();

		while (point.hasNext()) {
			current = point.next();

			if (max <= current.getX()) {
				max = current.getX();
			}
		}
		return max;
	}

	public int getMinX(){
		final Iterator<? extends Point> point = getPoints().iterator();
		Location current = point.next();
		int min = current.getX();

		while (point.hasNext()) {
			current = point.next();

			if (min > current.getX()) {
				min = current.getX();
			}
		}
		return min;
	}

	public int getMaxY(){
		final Iterator<? extends Point> point = getPoints().iterator();
		Location current = point.next();
		int max = current.getY();

		while (point.hasNext()) {
			current = point.next();

			if (max <= current.getY()) {
				max =current.getY();
			}
		}
		return max;
	}

	public int getMinY(){
		final Iterator<? extends Point> point = getPoints().iterator();
		Location current = point.next();
		int min = current.getX();

		while (point.hasNext()) {
			current = point.next();

			if (min > current.getX()) {
				min = current.getX();
			}
		}
		return min;
	}

	public int getWidth(){
		return(getMaxX() - getMinX());
	}

	public int getHeight(){
		return(getMaxY() - getMinY());
	}
}
